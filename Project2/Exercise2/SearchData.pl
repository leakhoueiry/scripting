#!/bin/usr/perl

use strict;
open ('FILE',"data.txt") || die "Can't open file!\n";
print "Reading from file \"data.txt\" \n";
my @data = <FILE>;
close FILE;

foreach(@data)
{
  my @line=split (/\t/, $_); #splitting each line by fields, a tab is the delimeter
  if ($line[2] =~ /^\d{4}(\d)\d{2}\1$/) #last 4 digits start and end with the same number, used back-references
  {
    #print $_; #just to shows the remaining suspects after the first condition

    if ($line[3] !~ /^[Dd]orms$/)#don't live in dorms
    {
      #print $_; #remaining suspects after second conditions

      if ($line[1] =~ /^[A-Za-z]{2,4}$/) #first name is between 2 and 4 characters
      {
        #print $_; #remaining suspects after third condition

        if ($line[0] =~ /^([A-z(\s)+(-)+]{7,})|([A-z]{6,})$/ )#last name has at least 6 characters, could be separated by space or by "-"

        {
          #print $_; #remaining suspects after fourth condition
          my @birthday = split (/\//, $line[4]);#splitting the date of birth, '/' is the delimeter

          if ($birthday[1] =~ /^1|10|11|12$/)#could be born in january, october, november or december
          {
            #print $_; #remaining suspects after fifth condition

            if ($birthday[2] =~ /^19(7[0-4]|[0-6]\d)/)#older than 45 years old, so born before 1975
            #considering that he isn't born before 1900
            {
              #print $_; #remaining suspects after sixth condition

              if ($birthday[0] =~ /^(\d)\1$/)#birthday has 2 same digits, using back-reference
              {
                #print $_; #remaining suspects after seventh condition

                if ($line[2] =~ /^20(20|1[89])\d{4}$/) #joined LAU after 2017, could be either 2018,2019 or 2020
                {
                  print "The robber is found!\n";
                  print "It is: ". $line[0]." ".$line[1]."\n";
                  print "His ID is: ".$line[2].", he lives in ".$line[3]." and his birth date is: ".$line[4]."\n";
                }
              }

            }
          }
        }
      }
    }

  }
}
