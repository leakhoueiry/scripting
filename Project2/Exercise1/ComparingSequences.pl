#!/bin/usr/perl

use strict;
#files are already in the current directory, no need to enter the path for QuerySequence.fasta and SequencesToCompare.fasta
my $nb_args =$#ARGV +1; #ARGV is used to read the input from the user on the terminal in the command line
my $queryFile;
my $sequencesFile;
my $writeFile;
#!!!! the user ha to enter the files in the command line in order, query sequence, sequences to compare then the file he wishes to write to it
if ($nb_args == 0)
{
  print "Enter the path of the file of the query sequence you wish to compare: ";
   $queryFile = <STDIN>;
  chomp $queryFile;
  print "\nEnter the path of the file that contains the sequences you want to compare to: ";
 $sequencesFile = <STDIN>;
  chomp $sequencesFile;
  print "Enter the path of the file where you want to store the sequences in order from most to least similar:";
   $writeFile = <STDIN>;
  chomp $writeFile;

}
elsif ($nb_args ==1)
{
   $queryFile = $ARGV[0];
  print "\nEnter the path of the file that contains the sequences you want to compare to: ";
   $sequencesFile = <STDIN>;
  chomp $sequencesFile;
  print "Enter the path of the file where you want to store the sequences in order from most to least similar:";
 $writeFile = <STDIN>;
  chomp $writeFile;
}
elsif ($nb_args ==2)
{
   $queryFile = $ARGV[0];
 $sequencesFile = $ARGV[1];
  print "Enter the path of the file where you want to store the sequences in order from most to least similar:";
   $writeFile = <STDIN>;
  chomp $writeFile;
}
else
{
   $queryFile = $ARGV[0];
   $sequencesFile = $ARGV[1];
   $writeFile = $ARGV[2];
}


#the following file containes the query sequence to compare
open ('SFILE', "$queryFile") || die "Can't open file, doesn't exist $!";
print "\nReading from file $queryFile \n";

my @sequence = <SFILE>; #stores the content of the file
close SFILE;
my $header = ''; #stores the header of the sequence
my $query_sequence =''; #stores the query sequence
foreach (@sequence)
{

  if ($_ =~ /^>(.*)/)
  {
    $header = $_;
  }
  else
  { $query_sequence .=$_;

  }
}

#The following file contain 9 sequences obtained after the translation (done with 3 reading frames)
#and the reverse transcription (where 3 possibilities were generated for each of the 3 sequences from the translation)
#the sequences are stored in the file "SequencesToCompare.fasta"
#The scripts needed for the translation and the reverse transcription are in the current directory
open ('CFILE', "$sequencesFile") || die "Can't open file, doesn't exist or wrong path! ";
#!!!!before reading from this file make sure that the first line is not empty
print "\nReading from file $sequencesFile \n";
my @sequences = <CFILE>; #stores the content of the file
close CFILE;

my $name = ''; #stores the header of each of the reversily transcribed sequences
my $other_sequences = ''; #store the sequences
my %sequences_compare = (); #hash that stores the name of the sequence with its respective sequence
foreach (@sequences)
{

  if ($_ =~ /^>(.*)/)
  {
    $name = $_;
    $other_sequences =''; #so the sequences don't concatenate
  }
  else
  { $other_sequences .=$_;
  $sequences_compare {$name} =  $other_sequences;
  }
}

my @each_sequence = keys %sequences_compare; #array storing the header of each sequence to compare

my %sequence_info=(); #hash that stores the total number of nucleotides (the maximum of the 3 sequences)
#for each sequence with its respective informations
foreach (@each_sequence)
{
  my $handling_sequence = $sequences_compare{$_}; #stores the sequence to handle
  my $n = $_;
  chomp $n;

  print "\nComparison of the query sequence \"" . $header. "\" to the sequence \"". $n . "\" \n";
  my @similarity;#array that stores the info of the sequence
  if ($_ =~ /^>RF:0.*/)
  {
    print "Starting at reading frame 0: \n";
     @similarity =ComparingSequences($query_sequence, $handling_sequence,0);
  }
  elsif ($_ =~ /^>RF:1.*/)
  {
    print "Starting at reading frame 1: \n";
     @similarity =ComparingSequences($query_sequence, $handling_sequence,1);
  }
  else
  {
    print "Starting at reading frame 2:\n";
     @similarity =ComparingSequences($query_sequence, $handling_sequence,2);
  }

   @similarity[5]=$_;#storing the name of the sequence at index 5
    my $reference =\@similarity;
    $sequence_info{$similarity[4]}= $reference;#hash that stores the total number of total similar
    #nucleotides as keys and the array of info as its info

}
my @total_nucleotides= keys %sequence_info;#array that stores the total number of similar @total_nucleotides
#for each sequence
my @sorted_array= sort {$b <=> $a}@total_nucleotides; #using the spaceship operator
#sorting the array in a decreasing order

open ('WFILE', '>'.$writeFile);#opening file to write the sequences to in
#order of most similar to least similar
foreach (@sorted_array)
{
  my $current=$sequence_info{$_};
  my @array_info = @$current; #dereferencing the array reference stored in the hash
  my $name1 = $array_info[5];#stores the name of the sequence
  chomp $name1;
  #header line of each sequences stores:
  #-its name:the starting reading frame it was translated and the possibility(1,2 or 3)after the reverse transcription
  #-the total number of similar nucleotides(TotalSimilar)and the number of similar A,C,G and T
  my $info_line = $name1." TotalSimilar:".$array_info[4]."|A=".$array_info[0]."|C=".$array_info[1]."|G=".$array_info[2]."|T=".$array_info[3]."\n";
  if ( !print WFILE $info_line) {warn "Can't write to file!\n";}#writing to file with details of similarity between the sequences
  my $curr_sequence= $sequences_compare{$array_info[5]};#accessing the sequence through the hash

  if ( !print WFILE $curr_sequence) {warn "Can't write to file!\n";}#writing sequence to file


}
print "Sequences are stored in order from most similar to least similar in the file $writeFile\n";
#sequences are stores in a fasta format (up to 60 characters per line)
#the original text files are already written in a fasta format
close WFILE;



sub ComparingSequences($$$)
{

  (my $query_sequence, my $handling_sequence,my  $j)= @_;
  #stores the number of similar nucleotide between the 2 sequences
  my $nb_A = 0;
  my $nb_C = 0;
  my $nb_G = 0;
  my $nb_T = 0;#incremented if 'U' is found also
  my $total_number = 0; #stores the total number of similar nucleotides found
  for (my $i=$j; $i<+length($query_sequence); $i=$i+1)

  {
    my $nucleotide = substr($query_sequence, $i, 1); #current  nucleotide in the query sequence
    my $nucleotide_compare = substr($handling_sequence, $i-$j,1); #current nucleotide in the sequence
                                          #to compare handling currently
                                          #taking into consideration the reading frame
                                          #always comparing the query sequency starting at index 'j'
                                          #and the handling sequence at index 0
  if ($nucleotide eq $nucleotide_compare)
  {
    $total_number = $total_number +1;
    if ($nucleotide eq 'A')
    {
      $nb_A = $nb_A +1;
    }
    elsif ($nucleotide eq 'C')
    {
      $nb_C = $nb_C +1;
    }
    elsif ($nucleotide eq 'G')
    {
      $nb_G = $nb_G +1;
    }
    else #nucleotide could be either 'U' or 'T'
    {
      $nb_T = $nb_T +1;
    }
  }

}
print "The total number of similar nucleotides is: " . $total_number. "
The number of similar A nucleotide is: ".$nb_A. "
The number of similar C nucleotide is: ". $nb_C."
The number of similar G nucleotide is ".$nb_G."
The number of similar T nucleotide is: ".$nb_T."\n";
return my @results = ($nb_A , $nb_C,$nb_G, $nb_T, $total_number);
}
