#!/bin/perl

use strict;

open ("FILE", "Protein.fasta") || die "Can't open file \n";
print "Reading from file \"Protein.fasta\" \n ";
#The protein sequences are retrieved from : http://crdd.osdd.net/raghava/bteval/allprot.html
my @protein = <FILE>;
close   FILE;
my %sequences =() ;
my $header= '';
my $protein_seq='';
foreach (@protein)
{
  if ($_ =~ /^>(.*)/)
  {
    $header = $_;
  }
  else
  { $protein_seq .=$_;
  $sequences {$header} =  $protein_seq;
}
}
#Amino acids are represented by letters as a fasta format
my %rev_code = ('S' => ['TCA', 'TCC', 'TCG' , 'TCT', 'AGC', 'AGT'], #Serine
                'F' => ['TTC','TTT'], #Phenylalanine
                'L'=> ['TTA', 'TTG', 'CTA', 'CTC', 'CTG', 'CTT'], #Leucine
                'Y'=> ['TAC', 'TAT'], #Tyrosine
                'C' => ['TGC', 'TGT'], #Cysteine
                'W' => ['TGG'], #Tryptophan
                'P' => ['CCA', 'CCC', 'CCG','CCT'], #Proline
                'Q' => ['CAA', 'CAG'], #Glutamine
                'R'=> ['CGA', 'CGC', 'CGG', 'CGT', 'AGA', 'AGG'], #Arginine
                'I' => ['ATA','ATC', 'ATT'], #Isoleucine
                'H' => ['CAT', 'CAC'], #Histidine
                'M'=> ['ATG'], #Methionine
                'T'=> ['ACA', 'ACC', 'ACG', 'ACT'], #Threonine
                'N'=> ['AAC', 'AAT'], #Asparagine
                'K'=> ['AAA', 'AAG'], #Lysine
                'V'=> ['GTA', 'GTC', 'GTG', 'GTT'], #Valine
                'A' => ['GCA', 'GCC', 'GCG', 'GCT'], #Alanine
                'D' => ['GAC', 'GAT'], #Aspartic acid
                'E' => ['GAA', 'GAG'], #Glutamic acid
                'G' => ['GGA', 'GGC', 'GGG', 'GGT'], #Glycine
                '_' => ['TAA', 'TAG', 'TGA'] #stop codon
                );

my @print_seq = keys %sequences; #array storing the header of each protein sequence

#the reverse transcription is going to reverse the transcription from
#a protein sequence to its corresponding DNA sequence
#However, because the genetic code is redundant,multiple DNA sequences are $possibilities
#Thus, the program is going to generate a random correspondant codon
#Different sequences are shown everytime the code is run
foreach (@print_seq)
{
  my $polypeptide = $sequences{$_}; #stores the polypeptide to handle
  print "\n Reverse Transcription of : $_\n ";
  for (my $i = 0; $i<length($polypeptide); $i=$i+1) #each amino acid is represented by 1 character
  {
    my $amino_acid = substr($polypeptide, $i,1);

    my @combination = keys %rev_code; #array that stores the amino acids
    foreach (@combination)
    {
      if ($_ eq $amino_acid)
      {
        my @possibilities = @{$rev_code{$_}}; #stores the array of possible corresponding codons
        my $random_codon = $possibilities [rand @possibilities]; #generating a corresponding codon at random
       print $random_codon;

        last;
      }
    }
  }

}
print "\n";
