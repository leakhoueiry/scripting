#!/bin/perl

use strict;
open ("FILE" ,"ACE2Sequences.fasta") || die "Can't open the file \n";
print "Reading from the file  \"ACE2Sequences.fasta\"  \n ";
my @sequences = <FILE>;
close FILE;
my %DNA=(); #hash that stores the header of each sequence with its respective sequence
my $header = ''; #stores the header of each line
my $seq =''; #store the sequence temporary to join the lines

foreach  (@sequences)
{
  if ($_ =~ /^>(.*)/) #when it encounters the header line, using
                  #the default variable because regular expressions
                  #act on $_ by default
  {  $header = $_;}
  else
  {
    $seq .= $_; #storing all the lines in one string
    $DNA{$header}= $seq;
  }

}
#genetic code, stop codon are represented by "_"
my %code= ('TCA'=>'S','TCC'=>'S',
'TCG'=>'S','TCT'=>'S','TTC'=>'F','TTT'=>'F',
'TTA'=>'L','TTG'=>'L','TAC'=>'Y',
'TAT'=>'Y','TAA'=>'_','TAG'=>'_','TGC'=>'C',
'TGT'=>'C','TGA'=>'_','TGG'=>'W','CTA'=>'L',
'CTC'=>'L','CTG'=>'L','CTT'=>'L','CCA'=>'P',
'CCC'=>'P','CCG'=>'P','CCT'=>'P','CAC'=>'H',
'CAT'=>'H','CAA'=>'Q','CAG'=>'Q','CGA'=>'R
CGC'=>'R','CGG'=>'R','CGT'=>'R','ATA'=>'I',
'ATC'=>'I','ATT'=>'I','ATG'=>'M','ACA'=>'T',
'ACC'=>'T','ACG'=>'T','ACT'=>'T','AAC'=>'N',
'AAT'=>'N','AAA'=>'K','AAG'=>'K','AGC'=>'S',
'AGT'=>'S','AGA'=>'R','AGG'=>'R','GTA'=>'V',
'GTC'=>'V','GTG'=>'V','GTT'=>'V','GCA'=>'A',
'GCC'=>'A','GCG'=>'A' ,'GCT'=>'A','GAC'=>'D',
'GAT'=>'D','GAA'=>'E','GAG'=>'E','GGA'=>'G',
'GGC'=>'G','GGG'=>'G','GGT'=>'G');
my @print_seq = keys %DNA;
#to make sure the sequences are saved into the hash
#foreach (@print_seq)
#{
#  print $_ . "\n" . $DNA{$_} . "\n";
#}
foreach (@print_seq)
{
my  $seq_handling = $DNA{$_}; #stores the sequence to handle

print "\n Translating the sequence: ". $_. "\n";
#taking into consideration the 3 different reading frames, thus 3 different protein products
#are possible
print "First possible translation: \n";
  for (my $i =0; $i< length($seq_handling); $i= $i+3) #starts at first nucleotide
  {
    my $codon =substr($seq_handling, $i, 3);
    #print "\n".$codon. "\n"; #just to make sure that it is working // to keep track
    my @combination = keys %code;
    foreach (@combination)
    {
      if ($_ eq $codon)
        {print $code{$_};
        last;}
      }
  }
  print "\n Second possible translation: \n";
  for (my $i =1; $i< length($seq_handling); $i= $i+3) #starts at second nucletide
  {
    my $codon =substr($seq_handling, $i, 3);
    #print "\n".$codon. "\n";
    my @combination = keys %code;
    foreach (@combination)
    {
      if ($_ eq $codon)
        {print $code{$_};
        last;}
      }
  }
  print "\n Third possible translation: \n"; #starts at third nucleotide
  for (my $i =2; $i< length($seq_handling); $i= $i+3)
  {
    my $codon =substr($seq_handling, $i, 3);
    #print "\n".$codon. "\n";
    my @combination = keys %code;
    foreach (@combination)
    {
      if ($_ eq $codon)
        {print $code{$_};
        last;}
      }
  }
  print "\n";

}
