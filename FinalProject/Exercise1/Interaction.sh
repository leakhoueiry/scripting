#!/usr/bin/bash


if [ $# -eq 0 ]
then
  read -p "No file entered, please enter the path to your file: " filename
elif [ $# -eq 1 ]
then
  filename=$1
else
  echo "Too many arguments"
  exit
fi
echo "Creating a copy of your file!"
cp $filename $filename.orig
total_interactions=$(tail -n+2 $filename | wc -l)
echo "The total number of interactions is $total_interactions"
sort $filename | uniq > uniqueInteractions.txt #removing duplicated lines
sort $filename | uniq -D > duplicateInteractions.txt #get duplicated lines
while read info
do
  #stores the delimiter of the variables, used by awk -F
  id_A=$(echo "$info"| awk -F \t ' {print $2} ') #stores the id of protein A
  id_B=$(echo "$info"| awk -F \t ' {print $11} ')#stores the id of protein B
  motif_A=$(echo "$info"| awk -F \t ' {print $5} ')#stores the motif of protein A
  motif_B=$(echo "$info"| awk -F \t ' {print $14} ')#stores the motif of protein B
  error_A=$(echo "$info"| awk -F \t ' {print $8} ')#stores the error rate of protein A
  error_B=$(echo "$info"| awk -F \t ' {print $17} ')#stores the error rate of protein B
  cost_A=$(echo "$info"| awk -F \t ' {print $9} ')#stores the cost of protein A
  cost_B=$(echo "$info"| awk -F \t ' {print $18} ')#stores the cost of protein B
  awk -F $del '{ if (($2==$id_B)&& ($11==$id_A)&&($5==$motif_B)&&($14==motif_A)&&($8 -eq $error_B)&&($17 -eq $error_A)&&($9 -eq $cost_B)&&($18 -eq $cost_A))
  line=$info }' $filename
    if [ -z $line ]
    then
      echo $line >> uniqueInteractions.txt #if line is empty, then it is unique
    else
      echo $line >> duplicateInteractions.txt #else it is a duplicate
    fi
done < $filename

nb_uniqueInt=$(wc -l uniqueInteractions.txt)
nb_dupInt=$(wc -l duplicateInteractions.txt)
echo "The number of unique interactions is: $nb_uniqueInt"
echo "The number of duplicates interactions is: $nb_dupInt"
