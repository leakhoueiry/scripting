#!/bin/usr/perl

use strict;

open ("IFILE", 'TransmembraneData.txt') || die "Can't open file! ";
#open ("IFILE", 'huu.txt') || die "Can't open file! ";
print "\nReading from file \"TransmembraneData.txt\"\n";

my @content = <IFILE>;
close IFILE;
my $nb_interactions = $#content;
#because this syntax gives me the last index of the aray (so size -1)
#but total nb of interactions is without the header line
print "The total number of interactions is ". $nb_interactions."\n";

#creating a backup of the original file
open ('BFILE', '>TrasmembraneData.txt.orig') || die "Can't wite to file!";
print "Creating a backup file \"TransmembraneData.txt.orig\"\n";
print BFILE @content;
close BFILE;

my %uniquehash;
my @duplicates;
foreach (@content)
{
  chomp $_;

  if (exists $uniquehash{$_})#checking if the entre already exists
  {push @duplicates ,$_;}
  else
  {$uniquehash{$_}=1;}
}
my @unique = keys %uniquehash;
#my $nb_unique = $#unique;
#print "The total nb of unique interactions is : ".$nb_unique."\n";
#my @duplicated;
my @unique_final;
my $found=0;
for (my $x=0; $x<=@unique; $x++)
{
chomp $unique[$x];
 for (my $y=$x+1; $y<=@unique; $y++)
  {
   chomp $unique[$y];
   $found = ComparingLines($unique[$x],$unique[$y]);
    if ($found ==1)
   {push @duplicates, $unique[$y];
     last;}
  }
  if ($found ==0)
  {
   push @unique_final, $unique[$x];
  }

}
sub ComparingLines($$)
{
  (my $lineA, my $lineB)= @_;
  my @fieldsA = split (/\t/, $lineA);
  my @fieldsB = split (/\t/, $lineB);
  if (($fieldsA[1]eq$fieldsB[10])&& ($fieldsB[1]eq$fieldsA[10]))
  {
    if (($fieldsA[4]eq$fieldsB[13])&& ($fieldsA[7]eq$fieldsB[16])&&($fieldsA[8]eq $fieldsB[17]))
    {
      if (($fieldsB[4]eq $fieldsA[13])&&($fieldsB[7]eq$fieldsA[16])&&($fieldsB[8]eq $fieldsA[17]))
      {return 1;}
    }
  }
  else
  {
    return 0;
  }
}
open ('WFILE','>uniqueInteractions.txt')||die "Can't write to file!";
print "Writing the unique interactions to \"uniqueInteractions.txt\"\n";
print WFILE @unique_final;
close WFILE;

open ('WFILE2', '>duplicateInteractions.txt')|| die "Can't write to file!";
print "Writing the duplicated Interactions to the file \"duplicateInteractions.txt\"\n";
print WFILE2 @duplicates;
close WFILE2;
