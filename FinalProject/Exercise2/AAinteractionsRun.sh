#!/bin/usr/bash

nb_files=$(ls -l PDBSumFiles/*.txt | wc -l)
echo "There are $nb_files files."

for file in PDBSumFiles/*.txt #joining all the files together and removing everythingthat does not include Non-bonded contacts
do
sed -n -e '/Non-bonded contacts/,$p' $file > NBC.txt
sed -e '/Disulphide bonds/,$d' NBC.txt | sed -e '/Salt bridges/,$d' >> NB.txt
done
grep -E -i '^.{3}\.' NB.txt > NBC.txt
#sed -n -e '/Non-bonded contacts/,$p' $file | sed -e '/Disulphide bonds/,$d' | sed -e '/Salt bridges/,$d' > NBC.txt
rm NB.txt
perl Int.pl #running perl scripts to find interactions
