#!/bin/usr/perl

use strict;
open ('FILE', 'NBC.txt')|| die "Can't open file! \n";

my @interaction = <FILE>;
close FILE;

my %nb_interactions=(); #hash that stores the interaction between amino acids
foreach(@interaction)
{
  chomp $_;
  my $AA1= substr($_, 17,3);
  my $nb1 = substr($_,22,3);
  my $AA2= substr($_,49,3);
  my $nb2 = substr($_,54,3);
  if (exists $nb_interactions{"$AA1-$AA2"})
  {
    my @arr=@{$nb_interactions{"$AA1-$AA2"}}; #check if the numbers the amino acids already exists or not, if they do, do not push them in the #array
    my @new =checkNumber($nb1,$nb2,@arr);
    @{$nb_interactions{"$AA1-$AA2"}}=@new;
  }
  else
  {
    my @arr2 = ("$nb1-$nb2");
    @{$nb_interactions{"$AA1-$AA2"}}= @arr2;

  }

}
open ('WFILE','>interactionStatistics.txt')|| die "Can't write to file!\n";
my $str1= "\tPHE\tLEU\tILE\tMET\tVAL\tSER\tPRO\tTHR\tALA\tTYR\tHIS\tGLN\tASN\tLYS\tASP\tGLU\tCYS\tTRP\tARG\tGLY";
print WFILE $str1;

my @line1= qw(PHE LEU ILE MET VAL SER PRO THR ALA TYR HIS GLN ASN LYS ASP GLU CYS TRP ARG GLY);
for (my $x=0;$x<@line1;$x++)
{
  print WFILE "\n".$line1[$x];
  my $output;
  my @elements;
  for (my $y=0;$y<@line1;$y++)
  {
    my $current= "$line1[$x]-$line1[$y]";
  
    @elements = @{$nb_interactions{$current}};

    my $nb = $#elements+1;#because it gives the index of last element so +1
    print WFILE "\t".$nb;
  }
}





sub checkNumber($$@) #subroutine to check whether the numbers of amino acids already exist in the array
{
  (my $num1,my $num2,my @ints)=@_;
  my $f=0;
  foreach(@ints)
  {
    if ($_ eq "$num1-$num2")
    {
      $f =1;
      last;

    }
  }
  if ($f ==0)
    {push @ints, "$num1-$num2";}
    return @ints;

}
