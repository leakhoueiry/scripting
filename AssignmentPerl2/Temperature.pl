#!/usr/bin/perl

print "Enter the outside temperature in degrees Fahrenheit:\n";
$tempF = <STDIN>;

#conversion to degrees Celsius
$tempC = (($tempF -32) * 5) / 9;
print "The temperature in degrees Celsius is ". $tempC ."\n";

if ($tempF < 40)
{print "Take a coat";}
else
{
  if ($tempF > 80)
  {print "Avoid sunburn \n";}
  else
  {print "It will be a great day! \n";}
}
