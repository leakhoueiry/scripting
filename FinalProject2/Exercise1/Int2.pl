#!/bin/usr/perl

use strict;
my $nb_args = $#ARGV +1; #ARGV is used to read the input from the user on the terminal in the command line
my $file;
if ($nb_args == 0)
{
  print "Enter the path of the file: ";#TransmembraneData.txt
   $file = <STDIN>;
  chomp $file;

}
else
{
  $file=$ARGV[0];
}


open ("IFILE", "$file") || die "Can't open file! ";
print "\nReading from file \"$file\"\n";

my @content = <IFILE>;
close IFILE;
my $nb_interactions = $#content;
#because this syntax gives me the last index of the aray (so size -1)
#but total nb of interactions is without the header line
print "The total number of interactions is ". $nb_interactions."\n";

#creating a backup of the original file
open ('BFILE', '>'.$file.'.orig') || die "Can't write to file!";
print "Creating a backup file \"$file.orig\"\n";
print BFILE @content;
close BFILE;

my %uniquehash;
my @duplicates;
foreach (@content)#first, we check if there is any duplicated lines
{
  chomp $_;

  if (exists $uniquehash{$_})#checking if the entry already exists
  {push @duplicates ,$_;}
  else
  {$uniquehash{$_}=1;}#if it doesn't exists, add it to the hash
}

my @unique_final;#array that stores the unique interactions after checing the duplicates interactions that are inversed
my %temphash; #temporary hash needed while looping
for (keys %uniquehash)
{
  chomp $_;
  my @fields= split (/\t/, $_);#splitting the file, tab is the delimiter
  my $curr=$fields[1].$fields[10].$fields[4].$fields[13].$fields[7].$fields[16].$fields[8].$fields[17];
  my $dup=$fields[10].$fields[1].$fields[13].$fields[4].$fields[16].$fields[7].$fields[17].$fields[8];#second possible interaction, where proteins are inversed
  if (exists $temphash{$dup})#check if they are present in the hash
  {
    push @duplicates , $_;#if it is, push in array of duplicates
  }
  else
  {
    $temphash{$curr}= $_;#if not add to hash
    push @unique_final, $_;#it is a unique interaction, push to unique array
  }
}
my $nb_unique=$#unique_final;#this file contains the header line,so the number of interactions is nb
#of lines -1  but this gives me the index of the last element of the array
print "The number of unique interaction is: ".$nb_unique."\n";
my $nb_dup= $#duplicates+1;#because this syntax gives me the indx of the last element of the array so the total nb of lines is +1
print "The number of duplicated interactions is: ".$nb_dup."\n";

open ('WFILE','>uniqueInteractions.txt')||die "Can't write to file!";
print "Writing the unique interactions to \"uniqueInteractions.txt\"\n";
print WFILE @unique_final;
close WFILE;

open ('WFILE2', '>duplicateInteractions.txt')|| die "Can't write to file!";
print "Writing the duplicated Interactions to the file \"duplicateInteractions.txt\"\n";
print WFILE2 @duplicates;
close WFILE2;
