#!/bin/usr/bash

#the files should be in the same dirctory as the script
nb_files=$(ls -l PDBSumFiles/*.txt | wc -l)
echo "There are $nb_files files."
echo "The number of interactions is going to be read from the file NBC.txt where all non-bonded contacts interactions are stored"
for file in PDBSumFiles/*.txt #joining all the files together and removing everythingthat does not include Non-bonded contacts
do
sed -n -e '/Non-bonded contacts/,$p' $file > NBC.txt
sed -e '/Disulphide bonds/,$d' NBC.txt | sed -e '/Salt bridges/,$d' >> NB.txt
done
grep -E -i '^.{3}\.' NB.txt > NBC.txt
#sed -n -e '/Non-bonded contacts/,$p' $file | sed -e '/Disulphide bonds/,$d' | sed -e '/Salt bridges/,$d' > NBC.txt
rm NB.txt
perl IntNoRun.pl #running perl scripts to find interactions
