#!/bin/#!/usr/bin/env bash
if [ $# -ne 2 ]; then
	# >&2 redirects to stderr
	echo "Usage: $0 [-f | -d |--directory] <something>" >$2
	exit 1
fi

case $1 in
-f)
	echo "working on file: $2"
	;;

	-d | --directory)
	echo "working on directory: $2"

	*)
		echo "Usage: $0 [-f |-d |--directory <something>" >&2

esac
