#!/usr/bin/perl

@list = (1,2,3,4,5,6,7,8,9,10);
@oddList = @list[0,2,4,6,8];
@primeList = @oddList[1,2,3];
print "The elements in the prime list are: \n";
print @primeList[0] . "\n";
print @primeList[1] . "\n";
print @primeList[2] . "\n";
print "The size of the initial list is: " . $#list. "\n";
print "The size of the odd list is: " . $#oddList . "\n";
print "The size of the prime list is: " . $#primeList . "\n";
