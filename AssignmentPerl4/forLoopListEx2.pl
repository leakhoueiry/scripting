#!/usr/bin/perl

@list = (1,2,3,4,5,6,7,8,9,10);
@oddList = @list[0,2,4,6,8];
@primeList = @oddList[1,2,3];
  print "The elements in the initial list are: \n";
foreach $x (@list)
{
  print $x . ", ";
}
print "\nThe elements in the odd list are: \n";

foreach $x (@oddList)
{

  print $x . ", ";
}

print "\nThe elements in the prime list are: \n";
foreach $x (@primeList)
{

  print $x . ", ";
}
print "\n";
