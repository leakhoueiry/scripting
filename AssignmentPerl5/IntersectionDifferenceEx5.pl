#!/bin/usr/perl

@list1 = qw(1 3 6 4 12 hi bye);
@list2 = qw (3 7 12 bye hey 5);
%temp=();
foreach (@list1)
{
  $temp{$_}=1;
}
@intersect = grep ($temp {$_}, @list2);
print "the intersection of the 2 arrays is: ";
foreach $x (@intersect)
{
  print $x . " ";
}
%temp = ();
foreach (@list1)
{
  $temp{$_}=1;
}
@difference = grep (!$temp {$_}, @list2);
print " \n the difference of the 2 arrays is: ";
foreach $y (@difference)
{
  print $y . " ";
}
print "\n Sorting the arrays\n";
@sorted1 = sort @intersect;
@sorted2 = sort @difference;
print "The first array sorted is: ";
foreach $x (@sorted1)
{
  print $x . " ";
}
print "\nThe second array sorted is : ";
foreach $y (@sorted2)
{
  print $y . " ";
}
print "\n";
