#!/usr/bin/perl

%cities = (London => "392H76",
            Paris => "BV362V6",
            Milano => "NVH76",
            Beirut => "786BH2");
print "Displaying the cities: \n";
for ( keys %cities)
{
  print $_ . " ";
}

  print "\n Choose a city: ";
  $input = <STDIN>;
  chomp $input;
  print "\n The corresponding zip code is:" . $cities{$input}. "\n";
  print "Reversing the hash... \n";
  %zipCodes= reverse %cities;
  print "Displaying the zip codes: \n";
for (keys %zipCodes)
{
  print $_ . " ";
}
  print "Choose a zipcode: ";
  $input2 = <STDIN>;
  chomp $input2;
  print "\n The corresponding city is: ". $zipCodes{$input2}. "\n";
