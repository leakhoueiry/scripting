#!/usr/bin/perl

%cities = (London => "392H76",
            Paris => "BV362V6",
            Milano => "NVH76",
            Beirut => "786BH2");
print "Displaying the cities: \n";
for ( keys %cities)
{
  print $_ . " ";
}
print "Do you want to add an entry? (yes/no)";
$choice = <STDIN>;
chomp $choice;
if ($choice eq "yes")
{
  print "Enter the name of the city to add:";
  $city =<STDIN>;
  chomp $city;
  print "\n Enter the zipcode to add:";
$zipcode= <STDIN>;
  chomp $zipcode;
  $cities {$city}= $zipcode;
}
for ( keys %cities)
{
  print $_ . " ";
}
print "Do you want to delete an entry? (yes/no)";
$choice2 = <STDIN>;
chomp $choice2;
if ($choice2 eq "yes")
{
  print "Enter the name of the city to delete:";
  $city2 =<STDIN>;
  chomp $city2;
  delete $cities{city2}
}
for ( keys %cities)
{
  print $_ . " ";
}

  print "\n Choose a city: ";
  $input = <STDIN>;
  chomp $input;
  if (exists $cities{$input})
  {
  print "\n The corresponding zip code is:" . $cities{$input}. "\n";
}
else{ print "The key you entered doesn't exist \n";}
print "Reversing the hash... \n";
  %zipCodes= reverse %cities;
  print "Displaying the zip codes: \n";
for (keys %zipCodes)
{
  print $_ . " ";
}
  print "Choose a zipcode: ";
  $input2 = <STDIN>;
  chomp $input2;
  if (exists $zipCodes{$input2})
  {print "\n The corresponding city is: ". $zipCodes{$input2}. "\n";}
else
{print "The zip code you entered doesn't exist \n";}
