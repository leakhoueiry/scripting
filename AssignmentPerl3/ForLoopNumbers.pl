#!/usr/bin/perl

print "Enter numbers, to stop, enter 13 \n";
print "The numbers are going to be added together. ";
print "9 and 18 aren't going to be included in the sum. \n";

$input= <STDIN>;
chomp $input;
$sum = $input;
while ($input != 13)
{
  print "Enter another number: \n";
  $input = <STDIN>;
  chomp $input;
  if (($input !=9) && ($input != 18))
  {$sum = $sum + $input;}

}
print "The total sum is: ". $sum . "\n";
