#!/bin/bash
if [ -ne 1 $# ]
then
	# >&2 redirects to stderr
	echo "Usage: $0 <age>" >&2
	exit 1
fi

age=$1

if [ -lt 18 $age ]
then
	echo 'Soft drink?'
elif [ -ge 18 $age -a -lt 21 $age ]
then
	echo 'Smoke?'
else
	echo 'Drink?'

fi
